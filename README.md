# pluma-themes

Collection of themes for pluma text editor, should work with gedit too.

# How to install:

Copy xml files to:

--> **/usr/share/gtksourceview-4/styles**

if not working copy also to:

--> **/usr/share/gtksourceview-3.0/styles/**